<?php

include 'pubs_dal.php'; 

$result = "UNDEFINED"; 

session_start(); 

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

	$req = $_POST['request']; 

	switch($req)
	{
		case "set_au_key":
			$_SESSION['last_au_key'] = $_POST['au_id']; 

			break; 

		case "get_last_au":
			// return author as JSON here instead of just author
			$result = $_SESSION['last_au_key']; 

			if (isset($_SESSION['last_au_key']))
			{
				$result = $_SESSION['last_au_key'];
			}
			else
			{
				$result = "NO Record";
			}

			// clear ID
			unset($_SESSION['last_au_key']);

			break; 

		case "get_author":
			$result = get_author($_POST['id']);
			break;

		case "get_authors":
			$result = get_all_authors(); 
			break; 

		case "get_au_books":
			$result = get_books_by_author($_POST['author']); 
			break; 

		case "add_author":
			$result = add_author($_POST['fname'], $_POST['lname'], $_POST['phone'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['contract']);
			break;

		case "delete_author":
			$result = remove_author($_POST['id']);
			break;

		case "update_author":
			error_reporting(E_ALL & ~E_NOTICE); // Ignore undefined index here--if they're undefined it'll be null which is what I want
			$result = update_author($_POST['id'], $_POST['lname'], $_POST['fname'], $_POST['phone'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['contract']);
			break;

		case "get_books_by_author":
			$result = get_books_by_author($_POST['id']);
			break;

		default: 
			$result = "unknown request: " . $req; 

	}// end switch on request

}else if($_SERVER['REQUEST_METHOD'] == 'GET'){

	echo "*cries*";

}

echo $result; 

?>
