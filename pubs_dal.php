<?php

function connect_to_pubs()
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=pubs'; 
	$user = 'root';
	$password = 'root'; 

	$handle = new PDO($dsn, $user, $password); 
	$handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

	return $handle; 
}

function get_author($id)
{
	$conn;
	try
	{
		$conn = connect_to_pubs();
	}
	catch(PDOException $ex)
	{
		return "open error: " . $ex->getMessage();
	}

	$sql = "SELECT * FROM authors WHERE au_id = ?";
	$stmt = $conn->prepare($sql);
	$retval;
	try
	{
		$stmt->execute(array($id));
		$rows = array();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$rows[] = $row;
		}
		$retval = json_encode($rows);
	}
	catch(PDOException $ex)
	{
		return "get_author(): unable to select author: " . $ex->getMessage();
	}

	$stmt->closeCursor();
	$stmt = null;
	$conn = null;
	return $retval;
}

function get_all_authors()
{
	$conn; 

	try
	{

		$conn = connect_to_pubs(); 

	}
	catch(PDOException $ex)
	{
		return "open error: " . $ex->getMessage();
	}


	$sql = 'CALL get_all_authors()'; // stored procedure 
	$sql = 'SELECT * FROM authors';

	$proc_get_authors = $conn->prepare($sql);
	
	try
	{

		$rs = $proc_get_authors->execute(); // result set = sql query 
	}
	catch(PDOException $ex)
	{
		$conn = null; // close connection 
		return "BadSql: " . $ex->getMessage();
	}

	$rows = array(); 

	while($row = $proc_get_authors->fetch(PDO::FETCH_ASSOC)){
		$rows[] = $row; // add row to array
	}

	$retVal = json_encode($rows); 
	$conn = null; // close connection 

	return $retVal; 

}// end get_all_authors
	
function get_books_by_author($au_id)
{

	$conn; 

	try
	{

		$conn = connect_to_pubs(); 

	}
	catch(PDOException $ex)
	{
		return "open error: " . mysqli_connect_error();
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "SELECT * FROM titles T JOIN titleauthor TA ON T.title_id = TA.title_id WHERE TA.au_id = ?"; 
	// PDO::prepare() loads ^ for use with PDO::execute()
	$stmt = $conn->prepare($sql);

	try
	{
		$stmt->execute(array($au_id)); 
		// PDO::execute(array) replaces each ? in ^ with its respective value in the array a la format string

		// exception not thrown - send as JSON

		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$rows[] = $row; // add each row to array
		}

		$retVal = json_encode($rows); 

	}
	catch(PDOException $e)
	{

		$retVal = "error getting books: " . $e->getMessage();
	}
	 
	// close connection 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal; 
		
}// end function get author info

function add_author($fname, $lname, $phone, $address, $city, $state, $zip, $contract)
{
	$conn;
	try
	{
		$conn = connect_to_pubs();
	}
	catch(PDOException $ex)
	{
		return "connect error: " . mysqli_connect_error() . "\n" . $ex->getMessage();
	}
	$conn->exec("SET CHARACTER SET utf8");
	$sql = "SELECT genid() AS genid";
	$stmt = $conn->prepare($sql);
	$id;
	try
	{
		$stmt->execute();
		$rows = array();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$rows[] = $row;
		}
		$jso = json_encode($rows);
		$obj = json_decode($jso);
		$id = $obj[0]->genid;
	}
	catch(PDOException $ex)
	{
		return "PDOException 143" . $ex->getMessage();
	}
	$sql = "INSERT INTO authors VALUES (?, ?, ?, ?, ?, ?, ?, ?, ";
	if($contract == 'true')
	{
		$sql = $sql . 'true)';
	}
	else
	{
		$sql = $sql . 'false)';
	}
	$stmt = $conn->prepare($sql);
	error_log("Adding author with (" . $id . ", " . $lname . ", " . $fname . ")");
	try
	{
		$stmt->execute(array($id, $lname, $fname, $phone, $address, $city, $state, $zip));
	}
	catch(PDOException $ex)
	{
		return "PDOException 158" . $ex->getMessage();
	}

	$sql = "SELECT * FROM authors WHERE au_id = ?";
	$stmt = $conn->prepare($sql);
	$retVal;
	try
	{
		$stmt->execute(array($id));
		$rows = array();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$rows[] = $row;
		}
		$retVal = json_encode($rows);
	}
	catch(PDOException $ex)
	{
		return "PDOException 170: " . $ex->getMessage();
	}
	$stmt->closeCursor();
	$stmt = null;
	$conn = null;
	return $retVal;

}

function remove_author($id)
{
	error_log("removing author");
	$conn;
	try
	{
		$conn = connect_to_pubs();
	}
	catch(PDOException $ex)
	{
		return "Connection error: " . mysqli_connect_error() . "\n" . $ex->getMessage();
	}
	$conn->exec("SET CHARACTER SET utf8");
	$sql = "DELETE FROM authors WHERE au_id = ?";
	$stmt = $conn->prepare($sql);
	try
	{
		$stmt->execute(array($id));
	}
	catch(PDOException $ex)
	{
		return "Failed to remove author: " . $ex->getMessage();
	}

	$stmt->closeCursor();
	$stmt = null;
	$conn = null;
	return "success?";
}

function update_author($id, $lname, $fname, $phone, $address, $city, $state, $zip, $contract)
{
	error_reporting(E_ALL);
	error_log("updating author");
	$conn;
	try
	{
		$conn = connect_to_pubs();
	}
	catch(PDOException $ex)
	{
		return "Failed to connect to pubs" . $ex->getMessage();
	}
	$conn->exec("SET CHARACTER SET utf8");
	$sql = "UPDATE authors SET ";
	// Add all not-null arguments to array
	$updating = array();
	if($lname != null || $lname != '')
	{
		$updating['au_lname'] = $lname;
	}
	if($fname != null || $fname != '')
	{
		$updating['au_fname'] = $fname;
	}
	if($phone != null || $phone != '')
	{
		$updating['phone'] = $phone;
	}
	if($address != null || $address != '')
	{
		$updating['address'] = $address;
	}
	if($city != null || $city != '')
	{
		$updating['city'] = $city;
	}
	if($state != null || $state != '')
	{
		$updating['state'] = $state;
	}
	if($zip != null || $zip != '')
	{
		$updating['zip'] = $zip;
	}
	if($contract != null || $contract != '')
	{
		$updating['contract'] = $contract;
	}
	
	// Now I need to make an array and intersplice the keys and values of $updating
	$interpolate = array();
	$first = true;
	foreach($updating as $key => $value)
	{
		error_log("Updating " . $key . " to " . $value);
		array_push($interpolate, $value);
		if($first)
		{
			$sql = $sql . $key . " = ? ";
			$first = false;
		}
		else
		{
			$sql = $sql . ", " . $key . " = ? ";
		}
	}

	if(count($updating) == 0)
	{
		return '';
	}

	$sql = $sql . " WHERE au_id = ?";

	error_log("Full SQL string: " . $sql);

	array_push($interpolate, $id);

	$stmt = $conn->prepare($sql);
	try
	{
		$stmt->execute($interpolate);
	}
	catch(PDOException $ex)
	{
		return "PDOException 276: " . $ex->getMessage();
	}

	$stmt->closeCursor();
	$stmt = null;
	$conn = null;
	return "";
}

/*function get_books_by_author($id)
{
	$conn;
	$retval;

	try
	{
		$conn = connect_to_pubs();
	}
	catch(PDOException $ex)
	{
		return "open error: " . $ex->getMessage();
	}
	$conn->exec("SET CHARACTER SET utf8");

	$sql = "SELECT * FROM TITLES WHERE au_id = ?";
	$stmt = $conn->prepare($sql);

	try
	{
		$stmt->execute(array($id));
		$rows = array();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$rows[] = $row;
		}

		$retval = json_encode($rows);
	}
	catch(PDOException $ex)
	{
		$retval = "error getting books from author " . $id . ": " . ex->getMessage();
	}

	$stmt->closeCursor();
	$stmt = null;
	$conn = null;
	return $retval;
}
 */
?> 
